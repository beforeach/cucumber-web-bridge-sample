@IncludeCustomJs:tinymce
Feature: CWB-variable

  Scenario: the use of variables
    Given i am on "http://across.foreach.be/demo/cwb/tinymce"
    And i save "My name is John Doe" as "variable1"
    And i save "#{browser.div('col-md-10').fetch('innerText')}" as "bodytext"
    And i save "content" as "editor"
    And i fill in tinymce "${editor}" with "${bodytext}"
    Then tinymce "${editor}" should contain "Testing an editor can be a challenging task"
    When i fill in tinymce "${editor}" with "${variable1}"
    Then tinymce "${editor}" should contain "My name is John Doe"

