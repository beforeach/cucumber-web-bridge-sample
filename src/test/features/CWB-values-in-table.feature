Feature: look up values in a table

  Scenario: look up values in table
    # this scenario checks data in a table to demonstrate the use of row, cell and table
    Given i am on "http://across.foreach.be/demo/cwb/tableoverview"
    Then row "/^1(.*)/" should contain "demo@localhost"
    And i should see cell "Demo user"
    And i should see cell "Demo 2" in row "/^2(.*)/"
    And cell "/^Demo u(.*)/" should contain "Demo user"


