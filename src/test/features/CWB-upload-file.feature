Feature: CWB-upload-file

  Scenario: upload a file
    Given i am on "http://across.foreach.be/demo/cwb/upload"
    # Will upload the file as test-file.jpeg - without javascript changes
    And i fill in the following:
      | type | locator | value                   |
      | file | file    | /files/logo-foreach.png |
    And i press "Upload"
    Then i should see row "/logo-foreach.png/" in table "file-details"
    And i should see row "/52721 bytes/" in table "file-details"

  Scenario: upload a file under a different name
    Given i am on "http://across.foreach.be/demo/cwb/upload"
    And i fill in the following:
      | type | locator | value                                     |
      | file | file    | test-image.png :: /files/logo-foreach.png |
    And i press "Upload"
    Then i should see row "/test-image.png/" in table "file-details"
    And i should see row "/52721 bytes/" in table "file-details"

  Scenario: File too large
    Given i am on "http://across.foreach.be/demo/cwb/upload"
    # Will upload the file as test-file.jpeg - without javascript changes
    And i fill in the following:
      | type | locator | value                   |
      | file | file    | /files/across.png       |
    And i press "Upload"
    # The file is too large so we will receive this error.
    And i should see paragraph "There has been an error processing your request"
