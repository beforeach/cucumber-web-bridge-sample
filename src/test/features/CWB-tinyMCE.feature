@resetBrowser
@IncludeCustomJs:tinymce
Feature: CWB-tinyMCE

  Background:
    Given i am on "http://across.foreach.be/demo/cwb/tinymce"
    And i save "content" as "editor"

  Scenario: Set and fetch html text
    When i fill in the following:
      | type    | locator   | value              |
      | tinymce | ${editor} | gezet vanuit tabel |
    Then the element values should contain:
      | type    | locator   | value        |
      | tinymce | ${editor} | gezet vanuit |
    When i fill in tinymce "${editor}" with "single line"
    Then tinymce "${editor}" should contain "single line"
    When i fill in tinymce "${editor}" with
    """
        <ul>
          <li>multiline</li>
          <li>text</li>
          <li>with <span class="kiekeboe">html 'pieces'</span></li>
        </ul>
        """
    Then tinymce "${editor}" should contain "with html 'pieces'"

  Scenario: Select, remove and replace text
    Given i fill in tinymce "${editor}" with "A single line of text."
    And tinymce "${editor}" contains "html:<p>A single line of text.</p>"
    And tinymce "${editor}" does not contain "html:<strong>single line</strong>"
    When i select text "single line" in tinymce "${editor}"
    And i click on link "/Bold/" in tinymceToolbar "${editor}"
    Then tinymce "${editor}" should contain "html:<strong>single line</strong>"
    When i replace text "line" with "number" in tinymce "${editor}"
    Then tinymce "${editor}" should contain "html:<strong>single number</strong>"
    When i delete text " of text" from tinymce "${editor}"
    Then tinymce "${editor}" should contain "A single number."

  Scenario: Add text to already present text
    Given i fill in tinymce "${editor}" with "Paragraph 1."
    When i insert text "Second sentence." in tinymce "${editor}"
    Then tinymce "${editor}" should contain "Paragraph 1.Second sentence."
    When i put the cursor before the text in tinymce "${editor}"
    And i insert text "Sentence before." in tinymce "${editor}"
    Then tinymce "${editor}" should equal "html:<p>Sentence before.Paragraph 1.Second sentence.</p>"
    When i put the cursor before the html in tinymce "${editor}"
    And i insert text "Paragraph before." in tinymce "${editor}"
    Then tinymce "${editor}" should contain "html:<p>Paragraph before.</p>"
    And tinymce "${editor}" should contain "/Paragraph before.+Sentence before/"

  Scenario: Explicit focus will put cursor at the beginning of the text
    Given i fill in tinymce "${editor}" with "Paragraph 1."
    When i put the focus on tinymce "${editor}"
    And i insert text "Second sentence." in tinymce "${editor}"
    Then tinymce "${editor}" should contain "Second sentence.Paragraph 1."

  Scenario: Insert text at a specific position
    Given i fill in tinymce "${editor}" with
    """
      Pagraph 1.
      Second sentence in one

      Paragraph 2.
    """
    When i put the cursor before text "one" in tinymce "${editor}"
    And i insert text "paragraph " in tinymce "${editor}"
    And i insert text "number " in tinymce "${editor}"
    Then tinymce "${editor}" should contain "Second sentence in paragraph number one"
    But tinymce "${editor}" should not contain "Second sentence in paragraph number one."
    When i put the cursor after text "one" in tinymce "${editor}"
    And i insert text "." in tinymce "${editor}"
    Then tinymce "${editor}" should contain "Second sentence in paragraph number one."

  Scenario: Select paragraph, remove tag contents, remove paragraph contents entirely, insert before and after
    Given i fill in tinymce "${editor}" with
    """
        <div>Paragraph 1.<ul><li>item 1</li><li>item 2</li></ul></div>
        <p>Paragraph 2.</p>
        """
    When i select the contents of the html node with text "item 1" in tinymce "${editor}"
    And i delete the selected text in tinymce "${editor}"
    Then tinymce "${editor}" should contain "html:<li></li>"
    When i select the html node with text "Paragraph 1" in tinymce "${editor}"
    And i delete the selection in tinymce "${editor}"
    Then tinymce "${editor}" should equal "html:<p>Paragraph 2.</p>"
    When i select the html node with text "Paragraph 2" in tinymce "${editor}"
    And i put the cursor before the selection in tinymce "${editor}"
    And i insert text "before " in tinymce "${editor}"
    And i select the html node with text "Paragraph 2" in tinymce "${editor}"
    And i put the cursor after the selection in tinymce "${editor}"
    And i insert text " after" in tinymce "${editor}"
    Then tinymce "${editor}" should equal "before Paragraph 2. after"