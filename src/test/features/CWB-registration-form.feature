Feature: CWB-registration-form

  Background:
    Given i am on "http://across.foreach.be/demo/cwb/registrationform"

  Scenario: Fill in the registration form
    # this scenario follows the happy flow, everything is filled in correctly
    Then i execute "#{browser.textbox('firstname').focus()}"
    And i fill in text "firstname" with "John"
    And i fill in text "lastname" with "Doe"
    And i select "15" from "date"
    And i select "june" from "month"
    And i select "1985" from "year"
    And i fill in text "street" with "ruestreet 12"
    And i fill in text "postalcode" with "1070"
    And i check checkbox "mailyesno"
    And i click on radio "sex[0]"
    And i click on button "submit"
    # After the submitting of the form we wait on the answer to verify the submitted values
    And i wait 1000 ms or until span "John" is visible
    And i should see span "/doe/i"
    And i should see span "15"
    And i should see span "june"
    And i should see span "1985"
    And i should see span "ruestreet 12"
    And i should see span "1070"
    And i should see span "true"
    And i should see span "male"
    And checkbox "mailyesno" is checked
    And radio "sex[0]" is checked

  Scenario: Do not fill in the registration form and submit
     # in this scenario nothing is filled in and I receive an error to fill in all the fields
    Then i execute "#{browser.submit('submit').focus()}"
    And i click on button "submit"
    And i should see div "Please fill in all the fields"

  Scenario: Fill in the registration form with wrong data
    # this scenario fills out every fields with wrong data and checks the printed errors
    Then i execute "#{browser.textbox('firstname').focus()}"
    Then i fill in text "firstname" with "1"
     # try to use * as lastname
    Then i fill in text "lastname" with "*"
     # try to use [ as a streetname
    And i fill in text "street" with "ruestreet [ "
     # try to use L as input for a postalcode
    And i fill in text "postalcode" with "1070L"

    And i select "15" from "date"
    And i select "june" from "month"
    And i select "1985" from "year"

    And i check checkbox "mailyesno"

    And i click on radio "sex[1]"
    And i click on button "submit"


    And i should see span "Error in firstname"
    And i should see span "Error in lastname"
    And i should see span "Error in street"
    And i should see span "Error in postalcode"

