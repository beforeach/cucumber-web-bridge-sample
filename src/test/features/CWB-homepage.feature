Feature: CWB-homepage

  Scenario: Go to the homepage of the CWB website
    Given i am on "http://across.foreach.com/demo/cwb"
    Then i should see link "Home"
    # I'm using highlight here to demonstrate on the screen that a certain link is found (or not) but in automated tests that no one visually
    # watches, it better to use i should see because highlight pauzes the test for a short period of time, so the execution of your tests will take longer
    And i highlight link "Registration form"
    And i highlight link "Upload a file"
    And i should see link "Rich text editor form"
    And i should see link "Pop up"