Feature: CWB-popup

  Scenario: handle popup
    Given i am on "http://across.foreach.be/demo/cwb/pop-up"
    And i click on link "Open this link in a pop up"
    Then i switch to popup "popuplink"
    And i should see div "This is a pop up"
    Then i clear the popup
    And i should see div "Link is open in a pop up"
    And i close popup "popuplink"

